## NOTES

### Rust

```
$ rustup self update
$ rustup update
```

```
$ cargo fmt
```

```
$ rustup component add clippy
$ cargo clippy
```

```
// make clippy pedantic, first line of main.rs
#![warn(clippy::all, clippy::pedantic)]
```

```
$ cargo update
```

```
$ cargo install --locked cargo-outdated
$ cargo outdated
```

```
$ cargo install -f cargo-audit
$ cargo audit
```

### Windows

```
# git-bash
$ eval `ssh-agent`
$ ssh -T git@gitlab.com

$ export GIT_SSH_COMMAND="C:/Program\ Files/Git/usr/bin/ssh.exe"
$ git --version
$ git version 2.31.1.windows.1
```