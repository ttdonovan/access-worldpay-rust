# Access Worldpay (Rust)

**Warning!** Experimental Access Worldpay Client (SDK) written in Rust.

https://developer.worldpay.com/docs/access-worldpay

## Development/Usage

```
$ cargo run -p simple-client --example simple
```

## Legacy API

Worldwide Payment Gateway (WPG)

https://developer.worldpay.com/docs/wpg

```
$ cargo run -p legacy-api-wpg --example basic
```
