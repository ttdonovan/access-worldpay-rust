use quick_xml::se::to_string;

use legacy_api_wpg::{
    config::Config,
    data::{Amount, Modification, OrderInqueryData, OrderModificationData},
    types::{InqueryType, ModifyType, PaymentServiceType},
    PaymentService, TEST_WORLDPAY_URL, VERSION,
};

fn main() {
    println!("Example: Basic");

    let config = Config::load(TEST_WORLDPAY_URL.into()).expect("failed to load config");

    let modification = PaymentService {
        version: VERSION.into(),
        merchant_code: config.worldpay_merchant_code.clone(),
        service_type: PaymentServiceType::Modify(Box::new(ModifyType::OrderModification(
            OrderModificationData {
                order_code: "YOUR_ORDER_CODE".into(),
                modification: Modification::Capture(Amount {
                    value: 1095,
                    currency_code: "USD".into(),
                    exponent: 2,
                    // debit_credit_indicator: "credit".into(),
                }),
            },
        ))),
    };

    // dbg!(&modification);

    let modification_xml = to_string(&modification).unwrap();
    dbg!(&modification_xml);

    let inquery = PaymentService {
        version: VERSION.into(),
        merchant_code: config.worldpay_merchant_code.clone(),
        service_type: PaymentServiceType::Inquery(Box::new(InqueryType::OrderInquery(OrderInqueryData {
            order_code: "YOUR_ORDER_CODE".into(),
        }))),
    };

    // dbg!(&inquery);

    let inquery_xml = to_string(&inquery).unwrap();
    dbg!(&inquery_xml);
}
