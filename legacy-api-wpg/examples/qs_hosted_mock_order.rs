// Integrate (Hosted) Quick Start: https://developer.worldpay.com/docs/wpg/hostedintegration/quickstart

use quick_xml::de::from_str;
use std::error::Error;

use legacy_api_wpg::{
    config::Config,
    data::{Address, Addressable, Amount, Include, OrderData, PaymentMethodMask, Shopper},
    helpers,
    response::PaymentService,
    Client, TEST_WORLDPAY_URL,
};

fn main() -> Result<(), Box<dyn Error>> {
    println!("Example: Quick Start Integrate (Hosted) Mock Order");

    let order_code = "YOUR_ORDER_CODE".to_string();
    let installation_id = "1234567".to_string(); // installationId identifies your Hosted Payment Page
    let description = "YOUR_DESCRIPTION".to_string();
    let amount = Amount {
        currency_code: "USD".into(),
        exponent: 2,
        value: 5000,
    };
    let shopper = Shopper {
        email: "john.doe@example.com".into(),
    };
    let address = Address {
        address_1: "123 Fake St".into(),
        postal_code: "94117".into(),
        city: "San Francisco".into(),
        state: "CA".into(),
        country_code: "US".into(),
    };

    let order = OrderData {
        order_code,
        installation_id,
        description,
        amount,
        order_content: "<![CDATA[MORE_DETAILS_HERE]]>".into(),
        payment_method_mask: PaymentMethodMask {
            include: Include { code: "ALL".into() },
        },
        shopper,
        shipping_address: Addressable::ShippingAddress(address.clone()),
        billing_address: Addressable::BillingAddress(address),
    };

    let config = Config::load(TEST_WORLDPAY_URL.into())?;
    // dbg!(&config);

    let service_type = helpers::build_payment_service_type_submit_order(order);
    let client = Client::with_config(config);
    let service_resp = client.payment_service(service_type).post()?;
    let resp = service_resp.resp;

    if resp.status().is_success() {
        let body = resp.text()?;
        dbg!(&body);

        let service: PaymentService = from_str(&body)?;
        dbg!(service);
    } else {
        dbg!(resp);
    }

    Ok(())
}
