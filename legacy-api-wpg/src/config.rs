use dotenv::dotenv;

use std::error::Error;

const ENV_WORLDPAY_MERCHANT_CODE: &str = "WOLRDPAY_MERCHANT_CODE";
const ENV_WORLDPAY_USERNAME: &str = "WORLDPAY_USERNAME";
const ENV_WORLDPAY_PASSWORD: &str = "WORLDPAY_PASSWORD";

#[derive(Debug, Clone)]
pub struct Config {
    pub worldpay_url: String,
    pub worldpay_merchant_code: String,
    pub worldpay_username: String,
    pub worldpay_password: String,
}

impl Config {
    pub fn load(worldpay_url: String) -> Result<Self, Box<dyn Error>> {
        dotenv().ok();

        let worldpay_merchant_code = dotenv::var(ENV_WORLDPAY_MERCHANT_CODE)?;
        let worldpay_username = dotenv::var(ENV_WORLDPAY_USERNAME)?;
        let worldpay_password = dotenv::var(ENV_WORLDPAY_PASSWORD)?;

        Ok(Config {
            worldpay_url,
            worldpay_merchant_code,
            worldpay_username,
            worldpay_password,
        })
    }
}
