use serde::Serialize;

#[derive(Debug, Serialize)]
#[serde(rename = "amount", rename_all = "camelCase")]
pub struct Amount {
    pub currency_code: String,
    pub exponent: u32,
    pub value: u32,
    // pub debit_credit_indicator: String,
}

// Inquery requests

#[derive(Debug, Serialize)]
#[serde(rename = "orderInquery", rename_all = "camelCase")]
pub struct OrderInqueryData {
    pub order_code: String,
}

#[derive(Debug, Serialize)]
#[serde(rename = "refundableAmountInquiry", rename_all = "camelCase")]
pub struct RefundableAmountInquiryData {
    pub order_code: String,
}

#[derive(Debug, Serialize)]
#[serde(rename = "PaymentOptionsInquiry", rename_all = "camelCase")]
pub struct PaymentOptionsInquiryData {
    pub country_code: String,
}

// Modify requests

#[derive(Debug, Serialize)]
pub enum Modification {
    #[serde(rename = "cancel")]
    Cancel, // TODO
    #[serde(rename = "capture")]
    Capture(Amount),
    #[serde(rename = "refund")]
    Refund, // TODO
}

#[derive(Debug, Serialize)]
#[serde(rename = "orderModification", rename_all = "camelCase")]
pub struct OrderModificationData {
    pub order_code: String,
    pub modification: Modification,
}

// Submit requests

#[derive(Debug, Serialize)]
#[serde(rename = "order", rename_all = "camelCase")]
pub struct OrderData {
    pub order_code: String,
    pub installation_id: String,
    #[serde(rename = "$unflatten=description")]
    pub description: String,
    pub amount: Amount,
    #[serde(rename = "$unflatten=orderContent")]
    pub order_content: String,
    pub payment_method_mask: PaymentMethodMask,
    pub shopper: Shopper,
    pub shipping_address: Addressable,
    pub billing_address: Addressable,
}

#[derive(Debug, Serialize)]
#[serde(rename = "paymentMethodMask")]
pub struct PaymentMethodMask {
    pub include: Include,
}

#[derive(Debug, Serialize)]
#[serde(rename = "include")]
pub struct Include {
    pub code: String,
}

#[derive(Debug, Serialize)]
#[serde(rename = "shopper")]
pub struct Shopper {
    #[serde(rename = "$unflatten=shopperEmailAddress")]
    pub email: String,
}

#[derive(Debug, Serialize)]
pub enum Addressable {
    #[serde(rename = "shippingAddress")]
    ShippingAddress(Address),
    #[serde(rename = "billingAddress")]
    BillingAddress(Address),
}

#[derive(Debug, Clone, Serialize)]
#[serde(rename = "address")]
pub struct Address {
    #[serde(rename = "$unflatten=address1")]
    pub address_1: String,
    #[serde(rename = "$unflatten=postalCode")]
    pub postal_code: String,
    #[serde(rename = "$unflatten=city")]
    pub city: String,
    #[serde(rename = "$unflatten=state")]
    pub state: String,
    #[serde(rename = "$unflatten=countryCode")]
    pub country_code: String,
}
