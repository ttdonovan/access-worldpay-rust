use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("http client error")]
    HttpClientError(#[from] reqwest::Error),
    #[error("xml error")]
    XmlError(#[from] quick_xml::de::DeError),
    #[error("unknown error")]
    Unknown,
}
