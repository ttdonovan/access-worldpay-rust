use crate::data::OrderData;
use crate::types::{PaymentServiceType, SubmitType};

pub fn build_payment_service_type_submit_order(order: OrderData) -> PaymentServiceType {
    PaymentServiceType::Submit(Box::new(SubmitType::Order(order)))
}
