use quick_xml::se::to_string;
use reqwest::{
    blocking::{Client as RClient, Response},
    header::{HeaderMap, CONTENT_TYPE},
};
use serde::Serialize;

pub mod config;
pub mod data;
pub mod error;
pub mod helpers;
pub mod response;
pub mod types;

use config::Config;
use error::Error;
use types::PaymentServiceType;

pub const VERSION: &str = "1.4";

pub const TEST_WORLDPAY_URL: &str =
    "https://secure-test.worldpay.com/jsp/merchant/xml/paymentService.jsp";

#[derive(Debug, Serialize)]
#[serde(rename = "paymentService", rename_all = "camelCase")]
pub struct PaymentService {
    pub version: String,
    pub merchant_code: String,
    pub service_type: PaymentServiceType, // or pub one_of: PaymentServiceType (?)
}

#[derive(Debug, Clone)]
pub struct Client {
    config: Config,
}

impl Client {
    pub fn with_config(config: Config) -> Self {
        Client { config }
    }

    pub fn payment_service(&self, service_type: PaymentServiceType) -> PaymentServiceRequest {
        let payment_service = PaymentService {
            version: VERSION.to_string(),
            merchant_code: self.config.worldpay_merchant_code.to_string(),
            service_type,
        };

        PaymentServiceRequest::new(self.clone(), payment_service)
    }
}

pub struct PaymentServiceRequest {
    client: Client,
    payment_service: PaymentService,
}

impl PaymentServiceRequest {
    fn new(client: Client, payment_service: PaymentService) -> Self {
        PaymentServiceRequest {
            client,
            payment_service,
        }
    }

    pub fn post(&self) -> Result<PaymentSerivceResponse, Error> {
        let config = &self.client.config;
        let service_xml = to_string(&self.payment_service)?;

        let client = RClient::new();
        let mut headers = HeaderMap::new();
        headers.insert(CONTENT_TYPE, "text/xml".parse().unwrap());

        let body = format!("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE paymentService PUBLIC \"-//Worldpay//DTD Worldpay PaymentService v1//EN\" \"http://dtd.worldpay.com/paymentService_v1.dtd\">{}", &service_xml);

        let resp = client
            .post(&config.worldpay_url)
            .basic_auth(&config.worldpay_username, Some(&config.worldpay_password))
            .headers(headers)
            .body(body)
            .send()?;

        Ok(PaymentSerivceResponse { resp })
    }
}

pub struct PaymentSerivceResponse {
    pub resp: Response,
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let result = 2 + 2;
        assert_eq!(result, 4);
    }
}
