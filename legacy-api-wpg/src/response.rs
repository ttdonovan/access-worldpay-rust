use serde::Deserialize;

#[derive(Debug, Deserialize)]
#[serde(rename = "paymentService", rename_all = "camelCase")]
pub struct PaymentService {
    pub version: String,
    pub merchant_code: String,
    pub reply: Reply,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Reply {
    #[serde(rename = "$value")]
    pub value: ReplyType,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum ReplyType {
    OrderStatus(OrderStatus),
    Error(Error),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct OrderStatus {
    pub order_code: String,
    #[serde(rename = "$value")]
    pub value: OrderStatusType,
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum OrderStatusType {
    Reference(Reference),
    Error(Error),
}

#[derive(Debug, Deserialize)]
pub struct Reference {
    pub id: String,
    #[serde(rename = "$value")]
    pub value: String,
}

#[derive(Debug, Deserialize)]
pub struct Error {
    pub code: String,
    // #[serde(rename = "$value")]
    // pub cdata: String,
}

#[cfg(test)]
mod tests {
    use super::{OrderStatus, OrderStatusType, PaymentService, Reply, ReplyType};
    use quick_xml::de::from_str;

    #[test]
    fn it_works_de_payment_serivce() {
        let xml = r#"
            <paymentService version="1.4" merchantCode="ExampleCode1">
                <reply>
                    <orderStatus orderCode="ExampleOrder1">
                        <reference id="YourReference">https://example.com</reference>
                    </orderStatus>
                </reply>
            </paymentService>
        "#;

        let payment_service: PaymentService = from_str(&xml).unwrap();

        assert_eq!(payment_service.merchant_code, "ExampleCode1");
    }

    #[test]
    fn it_works_de_order_status_reference() {
        let xml = r#"
            <orderStatus orderCode="ExampleOrder1">
                <reference id="YourReference">https://example.com</reference>
            </orderStatus>
        "#;

        let order_status: OrderStatus = from_str(&xml).unwrap();

        match order_status.value {
            OrderStatusType::Reference(r) => {
                assert_eq!(r.value, "https://example.com");
            }
            _ => unreachable!(),
        }
    }

    #[test]
    fn it_works_de_order_status_error() {
        let xml = r#"
            <reply>
                <error code="5">
                    <![CDATA[Invalid installation id]]>
                </error>
            </reply>
        "#;

        let order_status: Reply = from_str(&xml).unwrap();

        match order_status.value {
            ReplyType::Error(e) => {
                assert_eq!(e.code, "5");
            }
            _ => unreachable!(),
        }
    }
}
