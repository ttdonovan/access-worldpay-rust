use serde::Serialize;

use crate::data::*;

#[derive(Debug, Serialize)]
pub enum PaymentServiceType {
    #[serde(rename = "inquery")]
    Inquery(Box<InqueryType>),
    #[serde(rename = "modify")]
    Modify(Box<ModifyType>),
    #[serde(rename = "submit")]
    Submit(Box<SubmitType>),
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum InqueryType {
    OrderInquery(OrderInqueryData),
    RefundableAmountInquiry(RefundableAmountInquiryData),
    PaymentOptionsInquiry(PaymentOptionsInquiryData),
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum ModifyType {
    OrderModification(OrderModificationData),
}

#[derive(Debug, Serialize)]
#[serde(untagged)]
pub enum SubmitType {
    Order(OrderData),
}
