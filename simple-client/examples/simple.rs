use simple_client::api::Client;

fn main() -> Result<(), anyhow::Error> {
    println!("Example: Simple");

    let mut client = Client::new("https://try.access.worldpay.com/".into());

    let root = client.query_root()?;
    dbg!(&root);

    if let Some(payments) = client.query_service("service:payments")? {
        dbg!(&payments);

        // let events = client.query_service_links(&payments, "payments:events")?;
        // dbg!(&events);
    }

    Ok(())
}
