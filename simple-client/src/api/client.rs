use reqwest::blocking::Client as RClient;

use crate::error::Error;

use super::resources::{RootQuery, ServiceQuery};

pub struct Client {
    base_url: String,
    http: RClient,
    root: Option<RootQuery>,
}

impl Client {
    pub fn new(base_url: String) -> Self {
        let http = RClient::new();
        Client {
            base_url,
            http,
            root: None,
        }
    }

    pub fn query_root(&mut self) -> Result<RootQuery, Error> {
        let json = self.http.get(&self.base_url).send()?.text()?;

        let root: RootQuery = serde_json::from_str(&json)?;
        self.root = Some(root.clone());

        Ok(root)
    }

    pub fn query_service(&self, name: &str) -> Result<Option<ServiceQuery>, Error> {
        if let Some(root) = &self.root {
            let service = match root.get_link(name)? {
                Some(link) => {
                    let json = self.http.get(&link.href).send()?.text()?;

                    let service: ServiceQuery = serde_json::from_str(&json)?;

                    Some(service)
                }
                None => None,
            };

            Ok(service)
        } else {
            Ok(None)
        }
    }

    pub fn query_service_links(
        &self,
        service: &ServiceQuery,
        name: &str,
    ) -> Result<Option<ServiceQuery>, Error> {
        let service = match service.get_link(name)? {
            Some(link) => {
                let json = self.http.get(&link.href).send()?.text()?;

                dbg!(&json);

                let service: ServiceQuery = serde_json::from_str(&json)?;

                Some(service)
            }
            None => None,
        };

        Ok(service)
    }
}
