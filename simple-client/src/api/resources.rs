use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::error::Error;

#[derive(Debug, Deserialize, Serialize)]
pub struct Link {
    pub href: String,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct RootQuery {
    #[serde(rename = "_links")]
    pub links: Value,
}

impl RootQuery {
    pub fn get_link(&self, name: &str) -> Result<Option<Link>, Error> {
        match &self.links[name] {
            Value::Object(value) => {
                let link: Link = serde_json::from_value(Value::Object(value.clone()))?;
                Ok(Some(link))
            }
            _ => Ok(None),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct ServiceQuery {
    #[serde(rename = "_links")]
    pub links: Value,
}

impl ServiceQuery {
    pub fn get_link(&self, name: &str) -> Result<Option<Link>, Error> {
        match &self.links[name] {
            Value::Object(value) => {
                let link: Link = serde_json::from_value(Value::Object(value.clone()))?;
                Ok(Some(link))
            }
            _ => Ok(None),
        }
    }
}
