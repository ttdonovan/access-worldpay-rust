use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("http error")]
    Http(#[from] reqwest::Error),
    #[error("json error")]
    Json(#[from] serde_json::Error),
    #[error("unknown error")]
    Unknown,
}
